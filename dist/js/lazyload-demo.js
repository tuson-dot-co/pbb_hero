(function() {
  var LazyDemo,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  LazyDemo = (function() {
    function LazyDemo() {
      this.initDom = bind(this.initDom, this);
      this.doSection3 = bind(this.doSection3, this);
      this.doSection2 = bind(this.doSection2, this);
      this.doSection1 = bind(this.doSection1, this);
      this.doChanges = bind(this.doChanges, this);
      this.switchThrottleTo = bind(this.switchThrottleTo, this);
      this.calibrateSections = bind(this.calibrateSections, this);
      this.noop = bind(this.noop, this);
      this.initDom();
      TweenLite.defaultEase = Expo.easeOut;
      $((function(_this) {
        return function() {
          _this.sectionThrottled = {};
          _this.calibrateSections();
          _this.reCalibrateSections = function() {
            return _this.doCalibrate = setTimeout(function() {
              return _this.calibrateSections();
            }, 700);
          };
          $(window).resize(function() {
            clearTimeout(_this.doCalibrate);
            return _this.reCalibrateSections();
          });
          _this.doChanges();
          return setInterval(function() {
            return _this.doChanges();
          }, 100);
        };
      })(this));
    }

    LazyDemo.prototype.noop = function() {};

    LazyDemo.prototype.calibrateSections = function() {
      var j, len, ref, results, section, sectionNum;
      this.innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || this._window.height();
      this.sectionData = {};
      ref = this.sections;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        section = ref[j];
        sectionNum = $(section).attr('data-section-num');
        results.push(this.sectionData[sectionNum] = {
          offsetTop: $(section).offset().top,
          height: $(section).height(),
          offsetBottom: $(section).offset().top + $(section).height()
        });
      }
      return results;
    };

    LazyDemo.prototype.switchThrottleTo = function(num) {
      var i, j, len, ref, results, sec;
      ref = this.sections;
      results = [];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        sec = ref[i];
        if (num === i + 1) {
          results.push(this.sectionThrottled[i + 1] = true);
        } else {
          results.push(this.sectionThrottled[i + 1] = false);
        }
      }
      return results;
    };

    LazyDemo.prototype.doChanges = function() {
      var _offset, b, t;
      t = (window.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0);
      b = t + this.innerHeight;
      _offset = 200;
      if ((b > this.sectionData[1].offsetBottom - _offset) && (b < this.sectionData[2].offsetBottom - _offset) && this.sectionThrottled[1] !== true) {
        this.switchThrottleTo(1);
        return this.doSection1();
      }
      if ((b > this.sectionData[2].offsetBottom - _offset) && (b < this.sectionData[3].offsetBottom - _offset) && this.sectionThrottled[2] !== true) {
        this.switchThrottleTo(2);
        return this.doSection2();
      }
      if ((b > this.sectionData[3].offsetBottom - _offset) && this.sectionThrottled[3] !== true) {
        this.switchThrottleTo(3);
        return this.doSection3();
      }
    };

    LazyDemo.prototype.doSection1 = function() {
      var TL, copySpd, graphicSpd;
      this.doSection1 = this.noop;
      graphicSpd = 2;
      copySpd = 0.7;
      TL = new TimelineMax({
        delay: 0
      });
      return TL.to(this.graphics1, graphicSpd, {
        opacity: 1
      }).to(this.lazyphone1, graphicSpd, {
        x: 0
      }, "-=" + graphicSpd).to(this.lazyphone2, graphicSpd, {
        opacity: 1,
        x: 0
      }, "-=" + graphicSpd).to(this.copy1, copySpd, {
        opacity: 1,
        y: 0
      }, "-=" + (graphicSpd * 0.5)).to(this.cta, copySpd, {
        delay: 0.8,
        opacity: 1,
        x: 0
      }, "-=" + (graphicSpd * 0.8));
    };

    LazyDemo.prototype.doSection2 = function() {
      var TL, copySpd, graphicSpd;
      this.doSection2 = this.noop;
      graphicSpd = 2;
      copySpd = 0.7;
      TL = new TimelineMax({
        delay: 0
      });
      return TL.to(this.lazyphone3, graphicSpd, {
        opacity: 1,
        x: 0
      }).to(this.copy2, copySpd, {
        opacity: 1,
        y: 0
      }, "-=" + (graphicSpd * 0.8));
    };

    LazyDemo.prototype.doSection3 = function() {
      var TL, copySpd, graphicSpd;
      this.doSection3 = this.noop;
      graphicSpd = 2;
      copySpd = 0.7;
      TL = new TimelineMax({
        delay: 0
      });
      return TL.to(this.lazyphone4, graphicSpd, {
        opacity: 1,
        x: 0
      }).to(this.copy3, copySpd, {
        opacity: 1,
        y: 0
      }, "-=" + (graphicSpd * 0.8));
    };

    LazyDemo.prototype.initDom = function() {
      this.sections = $('.lazy-section');
      this.lazyphone1 = $('#lazyphone1');
      this.lazyphone2 = $('#lazyphone2');
      this.lazyphone3 = $('#lazyphone3');
      this.lazyphone4 = $('#lazyphone4');
      this.graphics1 = $('#graphics1');
      this.copy1 = $('#copy1');
      this.copy2 = $('#copy2');
      this.copy3 = $('#copy3');
      return this.cta = $('#cta');
    };

    return LazyDemo;

  })();

  this.LazyDemo = new LazyDemo;

}).call(this);
