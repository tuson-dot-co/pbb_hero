(function() {
  var HomepageHero,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  HomepageHero = (function() {
    function HomepageHero() {
      this.initDom = bind(this.initDom, this);
      this.oldBrowser = bind(this.oldBrowser, this);
      this.undoFinalLogo = bind(this.undoFinalLogo, this);
      this.doFinalLogo = bind(this.doFinalLogo, this);
      this.undoPizza = bind(this.undoPizza, this);
      this.doPizza = bind(this.doPizza, this);
      this.undoCoffee = bind(this.undoCoffee, this);
      this.doCoffee = bind(this.doCoffee, this);
      this.undoMobile = bind(this.undoMobile, this);
      this.doMobile = bind(this.doMobile, this);
      this.undoMainLogo = bind(this.undoMainLogo, this);
      this.doMainLogo = bind(this.doMainLogo, this);
      this.startHeroAnimation = bind(this.startHeroAnimation, this);
      this.isOldBrowser = this.oldBrowser();
      this.initDom();
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        this.isMobile = true;
      }
      $((function(_this) {
        return function() {
          var drawSpeed;
          if (!_this.isOldBrowser) {
            CSSPlugin.useSVGTransformAttr = true;
            drawSpeed = 35;
            _this.logoAnim = new Vivus('main-logo-extension', {
              duration: drawSpeed,
              delay: 0
            }).finish();
            _this.mobileAnim = new Vivus('mobile-surround', {
              type: 'oneByOne',
              duration: drawSpeed,
              delay: 0
            }).stop().reset();
            _this.coffeeAnim = new Vivus('coffee-cup', {
              type: 'oneByOne',
              duration: drawSpeed,
              delay: 0
            }).stop().reset();
            _this.pizzaAnim = new Vivus('pizza', {
              type: 'oneByOne',
              duration: drawSpeed,
              delay: 0
            }).stop().reset();
            _this.finalLogoAnim = new Vivus('final-logo-extension', {
              type: 'oneByOne',
              duration: drawSpeed,
              delay: 0
            }).stop().reset();
            TweenLite.set(_this.drawingsContainer, {
              display: 'block'
            });
          } else {
            _this.mainLogo.remove();
            _this.svgs.remove();
            TweenLite.set(_this.fallbackDrawings, {
              display: 'block'
            });
          }
          return TweenLite.to(_this.heroContent, 0.4, {
            opacity: 1,
            onStart: _this.startHeroAnimation
          });
        };
      })(this));
    }

    HomepageHero.prototype.startHeroAnimation = function() {
      var repeatNum, stateDwellTime, stateFadeTime, temp;
      this.finalDims = {
        scale: this.isMobile ? 0.3 : 0.5,
        y: this.isMobile ? 12 : 36,
        x: this.isMobile ? -97 : -170
      };
      stateDwellTime = 2;
      stateFadeTime = 1;
      repeatNum = -1;
      this.TL = new TimelineMax({
        delay: 0,
        repeat: repeatNum
      });
      temp = 0.3;
      return this.TL.from(this.initialState, stateFadeTime, {
        autoAlpha: 0
      }).from(this.mainLogo, stateFadeTime, {
        autoAlpha: 0
      }, "-=" + stateFadeTime).from(this.initialStateCopy, 0.3, {
        autoAlpha: 0,
        y: 0,
        ease: Power2.easeOut
      }, "-=" + stateFadeTime).to(this.mainLogo, temp, {
        scale: 0.5,
        x: -60,
        y: 67,
        onStart: this.undoMainLogo,
        ease: Power0.easeNone
      }, "+=" + stateDwellTime).to(this.drawingsContainer, temp, {
        y: 30,
        x: -20,
        ease: Power2.easeOut
      }, "-=" + temp).to(this.fallback1, 0.3, {
        opacity: 0
      }, "-=" + temp).set(this.mobileWrapElements, {
        x: 20
      }, "-=0.3").to(this.initialStateCopy, 0.3, {
        opacity: 0,
        y: 20,
        ease: Power2.easeOut
      }, "-=0.5").to(this.logoBase, 0.2, {
        opacity: 1
      }).to(this.mainLogo, 0.3, {
        opacity: 0,
        onComplete: this.doMainLogo
      }, "-=0.2").from(this.mobileState, stateFadeTime, {
        autoAlpha: 0,
        onStart: this.doMobile
      }).from(this.fallback2, 0.6, {
        autoAlpha: 0
      }, "-=" + stateFadeTime).from(this.mobileStateCopy, 0.1, {
        autoAlpha: 0,
        y: 0
      }, "-=" + (stateFadeTime * 0.5)).from(this.mobileStateScreens, 0.6, {
        autoAlpha: 0
      }, "-=0.6").to(this.mobileScreen1, 0.6, {
        opacity: 0
      }, "+=1").to(this.fallback2, 0.6, {
        opacity: 0
      }, "-=0.6").from(this.fallback3, 0.6, {
        autoAlpha: 0
      }, "-=0.6").to(this.mobileStateCopy, 0.3, {
        opacity: 0,
        y: 20,
        onStart: this.undoMobile
      }, "+=" + stateDwellTime).to(this.fallback3, 0.6, {
        opacity: 0
      }, "-=0.3").to(this.mobileStateScreens, 0.3, {
        opacity: 0
      }, "-=0.6").to(this.drawingsContainer, 0.4, {
        x: 25,
        y: 0
      }, "-=0.3").from(this.coffeeCupState, stateFadeTime, {
        autoAlpha: 0,
        onStart: this.doCoffee
      }, "-=0.4").from(this.fallback4, 0.6, {
        autoAlpha: 0
      }, "-=" + stateFadeTime).set(this.mobileState, {
        opacity: 0
      }).from(this.coffeeCupStateCopy, 0.1, {
        autoAlpha: 0,
        y: 0
      }, "-=" + (stateFadeTime * 0.5)).to(this.coffeeCupStateCopy, 0.3, {
        opacity: 0,
        y: 20,
        onStart: this.undoCoffee
      }, "+=" + stateDwellTime).to(this.fallback4, 0.6, {
        opacity: 0
      }, "-=0.3").to(this.drawingsContainer, 0.4, {
        x: 0
      }, "-=0.3").from(this.pizzaState, stateFadeTime, {
        autoAlpha: 0,
        onStart: this.doPizza
      }, "-=0.4").from(this.fallback5, 0.6, {
        autoAlpha: 0
      }, "-=" + stateFadeTime).from(this.pizzaStateCopy, 0.1, {
        autoAlpha: 0,
        y: 0
      }, "-=" + (stateFadeTime * 0.5)).to(this.pizzaStateCopy, 0.3, {
        opacity: 0,
        y: 20,
        onStart: this.undoPizza
      }, "+=" + stateDwellTime).to(this.fallback5, 0.6, {
        opacity: 0
      }, "-=0.3").set(this.coffeeCupState, {
        opacity: 0
      }).to(this.drawingsContainer, 0.3, {
        x: 0,
        y: 0
      }, "-=0.3").set(this.logoBase, {
        opacity: 0
      }).set(this.finalLogo, {
        opacity: 1,
        y: 67,
        x: -60,
        scale: 0.5
      }).to(this.finalLogo, 0.3, {
        scale: this.finalDims.scale,
        x: this.finalDims.x,
        y: this.finalDims.y,
        onStart: this.doFinalLogo
      }).from(this.paymark, 0.4, {
        autoAlpha: 0
      }).from(this.fallback6, 0.6, {
        autoAlpha: 0
      }, "-=0.4").from(this.finalStateCopy, 0.1, {
        autoAlpha: 0,
        y: 0
      }).to(this.paymark, 0.4, {
        opacity: 0
      }, "+=" + (stateDwellTime * 2)).to(this.finalLogo, 0.3, {
        opacity: 0
      }, "-=0.4").to(this.fallback6, 0.6, {
        opacity: 0
      }, "-=0.4").to(this.finalStateCopy, 0.3, {
        opacity: 0,
        y: 20
      }, "-=0.6").to(this.pizzaState, 1, {
        opacity: 0
      }, "-=0.3").to(this.fallback1, 0.6, {
        opacity: 1
      }, "-=0.3");
    };

    HomepageHero.prototype.doMainLogo = function() {
      if (!this.isOldBrowser) {
        return this.logoAnim.play(1);
      }
    };

    HomepageHero.prototype.undoMainLogo = function() {
      if (!this.isOldBrowser) {
        return this.logoAnim.play(-3);
      }
    };

    HomepageHero.prototype.doMobile = function() {
      if (!this.isOldBrowser) {
        return this.mobileAnim.reset().play(1);
      }
    };

    HomepageHero.prototype.undoMobile = function() {
      if (!this.isOldBrowser) {
        return this.mobileAnim.play(-3);
      }
    };

    HomepageHero.prototype.doCoffee = function() {
      if (!this.isOldBrowser) {
        return this.coffeeAnim.reset().play(1);
      }
    };

    HomepageHero.prototype.undoCoffee = function() {
      if (!this.isOldBrowser) {
        return this.coffeeAnim.play(-3);
      }
    };

    HomepageHero.prototype.doPizza = function() {
      if (!this.isOldBrowser) {
        return this.pizzaAnim.reset().play(1);
      }
    };

    HomepageHero.prototype.undoPizza = function() {
      if (!this.isOldBrowser) {
        return this.pizzaAnim.play(-3);
      }
    };

    HomepageHero.prototype.doFinalLogo = function() {
      if (!this.isOldBrowser) {
        return this.finalLogoAnim.reset().play(1);
      }
    };

    HomepageHero.prototype.undoFinalLogo = function() {
      if (!this.isOldBrowser) {
        return this.finalLogoAnim.play(-3);
      }
    };

    HomepageHero.prototype.oldBrowser = function() {
      var Idx, sAgent;
      sAgent = window.navigator.userAgent;
      Idx = sAgent.indexOf("MSIE");
      if ((Idx > 0) || (!!navigator.userAgent.match(/Trident\/7\./))) {
        return true;
      } else {
        return false;
      }
    };

    HomepageHero.prototype.initDom = function() {
      this.pause = $('#pause');
      this.play = $('#play');
      this.heroContent = $('#hero-header');
      this.heroStates = $('#hero-states-container .states');
      this.initialState = $('#initial-state');
      this.initialStateCopy = $('#initial-state h3');
      this.mobileState = $('#mobile-state');
      this.mobileStateCopy = $('#mobile-state h3');
      this.mobileWrapElements = $('.exception');
      this.mobileStateScreens = $('#hero-phone-screens');
      this.mobileScreen1 = $('#mob-screen-1');
      this.mobileScreen2 = $('#mob-screen-2');
      this.coffeeCupState = $('#coffee-cup-state');
      this.coffeeCupStateCopy = $('#coffee-cup-state h3');
      this.pizzaState = $('#pizza-state');
      this.pizzaStateCopy = $('#pizza-state h3');
      this.finalState = $('#final-state');
      this.finalStateCopy = $('#final-state h3');
      this.paymark = $('#paymark');
      this.drawingsContainer = $('#drawings');
      this.mainLogo = $('#main-logo-wrap');
      this.finalLogo = $('#final-logo-wrap');
      this.svgs = $('.icon-elems');
      this.logoBase = $('#drawings-logo-base');
      this.fallbackDrawings = $('#fallback-drawings');
      this.fallback1 = this.fallbackDrawings.children('#fallback1');
      this.fallback2 = this.fallbackDrawings.children('#fallback2');
      this.fallback3 = this.fallbackDrawings.children('#fallback3');
      this.fallback4 = this.fallbackDrawings.children('#fallback4');
      this.fallback5 = this.fallbackDrawings.children('#fallback5');
      return this.fallback6 = this.fallbackDrawings.children('#fallback6');
    };

    return HomepageHero;

  })();

  this.HomepageHero = new HomepageHero;

}).call(this);
