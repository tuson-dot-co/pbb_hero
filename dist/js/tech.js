(function() {
  var TechJourney,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  TechJourney = (function() {
    function TechJourney() {
      this.initDom = bind(this.initDom, this);
      this.showEndpoints = bind(this.showEndpoints, this);
      this.showRoute3B = bind(this.showRoute3B, this);
      this.showRoute3A = bind(this.showRoute3A, this);
      this.colouriseButton = bind(this.colouriseButton, this);
      this.flattenButtons = bind(this.flattenButtons, this);
      this.doRoadSwitch = bind(this.doRoadSwitch, this);
      this.resetException = bind(this.resetException, this);
      this.showCopy5 = bind(this.showCopy5, this);
      this.showCopy4 = bind(this.showCopy4, this);
      this.showCopy3b = bind(this.showCopy3b, this);
      this.showCopy3a = bind(this.showCopy3a, this);
      this.showCopy2 = bind(this.showCopy2, this);
      this.showCopy1 = bind(this.showCopy1, this);
      this.showToken = bind(this.showToken, this);
      this.showTransaction = bind(this.showTransaction, this);
      this.configureProgressAnimations = bind(this.configureProgressAnimations, this);
      this.wobbleMarker = bind(this.wobbleMarker, this);
      this.step3infoBox = bind(this.step3infoBox, this);
      this.step3stuff = bind(this.step3stuff, this);
      this.finishStuff = bind(this.finishStuff, this);
      this.doChanges = bind(this.doChanges, this);
      this.switchThrottleTo = bind(this.switchThrottleTo, this);
      this.calibrateSteps = bind(this.calibrateSteps, this);
      this.noop = bind(this.noop, this);
      this.initDom();
      this.innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || this._window.height();
      this.isMobile = false;
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        this.isMobile = true;
      }
      this._EXCEPTION = false;
      this._ORANGE = '#ff6e00';
      this._GREY = '#898989';
      $((function(_this) {
        return function() {
          _this.showCopy1();
          TweenLite.defaultEase = Power0.easeNone;
          _this.calibrateSteps();
          _this.reCalibrateSteps = function() {
            return _this.doCalibrate = setTimeout(function() {
              return _this.calibrateSteps();
            }, 700);
          };
          $(window).resize(function() {
            clearTimeout(_this.doCalibrate);
            return _this.reCalibrateSteps();
          });
          _this.configureProgressAnimations();
          _this.stepThrottled = {};
          _this.doChanges();
          setInterval(function() {
            return _this.doChanges();
          }, 100);
          if (_this.isMobile) {
            _this.yesBtn.on('click', _this.showRoute3A);
            return _this.noBtn.on('click', _this.showRoute3B);
          }
        };
      })(this));
    }

    TechJourney.prototype.noop = function() {};

    TechJourney.prototype.calibrateSteps = function() {
      var j, len, ref, results, step, stepNum;
      this.innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || this._window.height();
      this.stepData = {};
      ref = this.steps;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        step = ref[j];
        stepNum = $(step).attr('data-step-num');
        results.push(this.stepData[stepNum] = {
          offsetTop: $(step).offset().top,
          height: $(step).height(),
          offsetBottom: $(step).offset().top + $(step).height()
        });
      }
      return results;
    };

    TechJourney.prototype.switchThrottleTo = function(num) {
      var i, j, len, ref, results, step;
      ref = this.steps;
      results = [];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        step = ref[i];
        if (num === i + 1) {
          results.push(this.stepThrottled[i + 1] = true);
        } else {
          results.push(this.stepThrottled[i + 1] = false);
        }
      }
      return results;
    };

    TechJourney.prototype.doChanges = function() {
      var _offset, b, t;
      t = (window.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0);
      b = t + this.innerHeight;
      _offset = 150;
      if ((t < this.stepData[1].offsetTop - _offset) && this.stepThrottled[1] !== true) {
        this.switchThrottleTo(1);
        this._PROGRESS = 1;
        return this.TL.tweenTo('step 1', {
          onComplete: this.wobbleMarker
        });
      }
      if ((t > this.stepData[1].offsetTop - _offset) && (t < this.stepData[2].offsetTop - _offset) && this.stepThrottled[2] !== true) {
        this.switchThrottleTo(2);
        this._PROGRESS = 2;
        this.showToken();
        this.showCopy2();
        return this.TL.tweenTo('step 2', {
          onComplete: this.wobbleMarker
        });
      }
      if ((t > this.stepData[2].offsetTop - _offset) && (t < this.stepData[3].offsetTop - _offset) && this.stepThrottled[3] !== true) {
        this.switchThrottleTo(3);
        this._PROGRESS = 3;
        this.flattenButtons();
        return this.TL.tweenTo('step 3', {
          onComplete: this.step3stuff
        });
      }
      if ((t > this.stepData[3].offsetTop - _offset) && (t < this.stepData[4].offsetTop - _offset) && this.stepThrottled[4] !== true && this._EXCEPTION !== true) {
        this.switchThrottleTo(4);
        this._PROGRESS = 4;
        this.colouriseButton();
        this.showCopy3a();
        if (!this.isMobile) {
          this.showCopy3b(0.6);
        }
        return this.TL.tweenTo('step 4', {
          onComplete: this.wobbleMarker
        });
      }
      if ((t > this.stepData[4].offsetTop - _offset) && (t < this.stepData[5].offsetTop - _offset) && this.stepThrottled[5] !== true) {
        this.switchThrottleTo(5);
        this._PROGRESS = 5;
        this.showTransaction();
        this.showCopy4();
        return this.TL.tweenTo('step 5', {
          onComplete: this.wobbleMarker
        });
      }
      if ((t > this.stepData[5].offsetTop - _offset) && (t < (this.stepData[6].offsetTop - (this.stepData[5].height * 0.7))) && this.stepThrottled[6] !== true) {
        this.switchThrottleTo(6);
        this._PROGRESS = 6;
        return this.TL.tweenTo('finish', {
          onComplete: this.finishStuff
        });
      }
    };

    TechJourney.prototype.finishStuff = function() {
      return this.showEndpoints();
    };

    TechJourney.prototype.step3stuff = function() {
      this.wobbleMarker();
      return this.step3infoBox();
    };

    TechJourney.prototype.step3infoBox = function() {
      var anim, spd;
      this.step3infoBox = this.noop;
      anim = new TimelineMax({
        delay: 0.2
      });
      spd = 0.4;
      return anim.to(this.step3CopyWrap, spd, {
        scale: 1,
        opacity: 1,
        ease: Power2.easeOut
      }).to(this.step3Copy, spd, {
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.wobbleMarker = function() {
      var el;
      if (this._PROGRESS < 7) {
        el = $("#marker" + this._PROGRESS);
        TweenMax.to(el, .1, {
          scale: 1.1,
          ease: Quad.easeInOut
        });
        TweenMax.to(el, .1, {
          scale: 1,
          delay: .1
        });
      }
      if (this._PROGRESS === 6 && !this.isMobile) {
        this.showCopy5(2 - 2);
        el = $("#marker6dt");
        TweenMax.to(el, .1, {
          scale: 1.1,
          ease: Quad.easeInOut
        });
        return TweenMax.to(el, .1, {
          scale: 1,
          delay: .1
        });
      }
    };

    TechJourney.prototype.configureProgressAnimations = function() {
      var _line, _road, _speed, i, j, len, ref, results, road, step;
      this.TL = new TimelineMax({
        delay: 0
      }).pause();
      _speed = 0.12;
      ref = this.steps;
      results = [];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        step = ref[i];
        this.TL.addLabel("step " + (i + 1));
        if ((i + 1) < 6) {
          results.push((function() {
            var k, len1, ref1, results1;
            ref1 = $(step).children('.roads');
            results1 = [];
            for (k = 0, len1 = ref1.length; k < len1; k++) {
              road = ref1[k];
              _road = $(road);
              _line = _road.children('.prog-line');
              if (_road.hasClass('vert')) {
                results1.push(this.TL.append(TweenLite.to(_line, _speed, {
                  height: '100%'
                })));
              } else {
                results1.push(this.TL.append(TweenLite.to(_line, _speed, {
                  width: '100%'
                })));
              }
            }
            return results1;
          }).call(this));
        } else if ((i + 1) === 6) {
          if (this.isMobile) {
            this.TL.append(TweenLite.to(this.endRoads1a, _speed, {
              width: '100%',
              onStart: this.wobbleMarker
            }));
            this.TL.append(TweenLite.to(this.endRoads1b, _speed, {
              width: '100%',
              delay: -_speed
            }));
            this.TL.append(TweenLite.to(this.endRoads2a, _speed, {
              height: '100%'
            }));
            this.TL.append(TweenLite.to(this.endRoads2b, _speed, {
              height: '100%',
              delay: -_speed
            }));
            this.TL.append(TweenLite.to(this.endRoads2c, _speed, {
              height: '100%',
              delay: -_speed
            }));
          } else {
            this.TL.append(TweenLite.to(this.endRoads0, _speed, {
              height: '100%',
              onStart: this.wobbleMarker
            }));
            this.TL.append(TweenLite.to(this.endRoads1a, _speed, {
              width: '100%'
            }));
            this.TL.append(TweenLite.to(this.endRoads1b, _speed, {
              width: '100%',
              delay: -_speed
            }));
            this.TL.append(TweenLite.to(this.endRoads2a, _speed, {
              height: '100%'
            }));
            this.TL.append(TweenLite.to(this.endRoads2b, _speed, {
              height: '100%',
              delay: -_speed * 2
            }));
            this.TL.append(TweenLite.to(this.endRoads2c, _speed, {
              height: '100%',
              delay: -_speed
            }));
          }
          results.push(this.TL.addLabel("finish"));
        } else {
          results.push(void 0);
        }
      }
      return results;
    };

    TechJourney.prototype.showTransaction = function() {
      var anim, spd;
      this.showTransaction = this.noop;
      anim = new TimelineMax({
        delay: 1
      });
      spd = 0.6;
      return anim.to(this.techBank1, 0.4, {
        y: 0,
        opacity: 1,
        ease: Power2.easeOut
      }).to(this.techShop, 0.4, {
        y: 0,
        opacity: 1,
        ease: Power2.easeOut
      }).set(this.techToken2, {
        opacity: 1
      }).to(this.techToken2, 1.8, {
        y: '300%',
        rotation: 35,
        ease: Power3.easeIn
      }, "-=0.4").to(this.techToken2, 0.6, {
        y: '195%',
        rotation: 0,
        ease: Back.easeOut.config(1.7)
      }).to(this.techTick, 0.3, {
        scale: 1,
        opacity: 1,
        ease: Back.easeOut.config(1.7)
      }, "-=0.6").to(this.techDownArrow, 0.3, {
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      }, "+=0.3");
    };

    TechJourney.prototype.showToken = function() {
      var anim, spd;
      this.showToken = this.noop;
      anim = new TimelineMax({
        delay: 0
      });
      spd = 0.6;
      return anim.to(this.basketImg, spd, {
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      }).to(this.arrowImg, spd, {
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      }, "-=0.3").to(this.tokenImg, spd, {
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      }, "-=0.3");
    };

    TechJourney.prototype.showCopy1 = function() {
      this.showCopy1 = this.noop;
      TweenLite.to(this.laptop, 0.7, {
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
      return TweenLite.to(this.copy1, 0.7, {
        delay: 0.5,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.showCopy2 = function() {
      this.showCopy2 = this.noop;
      return TweenLite.to(this.copy2, 0.7, {
        delay: 0.6,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.showCopy3a = function() {
      this.showCopy3a = this.noop;
      TweenLite.to(this.copy3a, 0.7, {
        delay: 0.6,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
      return TweenLite.to(this.pingPhone, 0.7, {
        delay: 1,
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.showCopy3b = function(dly) {
      if (dly == null) {
        dly = 0;
      }
      this.showCopy3b = this.noop;
      TweenLite.to(this.copy3b, 0.7, {
        delay: dly,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
      if (this.isMobile) {
        return TweenLite.to(this.noappPhone, 0.7, {
          delay: 0.6,
          opacity: 1,
          x: 0,
          ease: Power2.easeOut
        });
      }
    };

    TechJourney.prototype.showCopy4 = function() {
      this.showCopy4 = this.noop;
      return TweenLite.to(this.copy4, 0.7, {
        delay: 0.6,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.showCopy5 = function(dly) {
      if (dly == null) {
        dly = 1;
      }
      this.showCopy5 = this.noop;
      return TweenLite.to(this.copy5, 0.7, {
        delay: dly,
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      });
    };

    TechJourney.prototype.resetException = function() {
      return this._EXCEPTION = false;
    };

    TechJourney.prototype.doRoadSwitch = function() {
      if (this.step3.hasClass('route-a')) {
        this.step3.removeClass('route-a');
        this.step3.addClass('route-b');
        this.step4.removeClass('route-a');
        return this.step4.addClass('route-b');
      } else {
        this.step3.addClass('route-a');
        this.step3.removeClass('route-b');
        this.step4.removeClass('route-b');
        return this.step4.addClass('route-a');
      }
    };

    TechJourney.prototype.flattenButtons = function() {
      return TweenLite.to($('button.options'), 0.1, {
        css: {
          backgroundColor: this._GREY
        }
      });
    };

    TechJourney.prototype.colouriseButton = function() {
      return TweenLite.to($('button.options.selected'), 0.2, {
        delay: 0.1,
        css: {
          backgroundColor: this._ORANGE
        }
      });
    };

    TechJourney.prototype.showRoute3A = function() {
      var anim;
      if (this.step3.hasClass('route-a')) {
        return;
      }
      this.noBtn.removeClass('selected');
      this.yesBtn.addClass('selected');
      this._EXCEPTION = true;
      this.flattenButtons();
      this.switchThrottleTo(3);
      this._PROGRESS = 3;
      this.TL.tweenTo('step 3').duration(0.15);
      anim = new TimelineLite({
        delay: 0.1
      });
      return anim.to(this.copy3b, 0.4, {
        opacity: 0,
        y: -10,
        ease: Power2.easeOut
      }).to(this.noappPhone, 0.4, {
        opacity: 0,
        x: -60,
        ease: Power2.easeOut
      }, "-=0.4").to(this.switchRoad, 0.4, {
        opacity: 0,
        ease: Power2.easeOut,
        onComplete: this.doRoadSwitch
      }, "-=0.4").to(this.switchRoad, 0.4, {
        opacity: 1,
        ease: Power2.easeOut,
        onComplete: this.resetException
      }).to(this.copy3a, 0.4, {
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      }).to(this.pingPhone, 0.4, {
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      }, "-=0.4");
    };

    TechJourney.prototype.showRoute3B = function() {
      var anim;
      if (this.step3.hasClass('route-b')) {
        return;
      }
      this._EXCEPTION = true;
      this.flattenButtons();
      this.yesBtn.removeClass('selected');
      this.noBtn.addClass('selected');
      this.switchThrottleTo(3);
      this._PROGRESS = 3;
      this.TL.tweenTo('step 3').duration(0.15);
      anim = new TimelineLite({
        delay: 0.1
      });
      return anim.to(this.copy3a, 0.4, {
        opacity: 0,
        y: -10,
        ease: Power2.easeOut
      }).to(this.pingPhone, 0.4, {
        opacity: 0,
        x: 60,
        ease: Power2.easeOut
      }, "-=0.4").to(this.switchRoad, 0.4, {
        opacity: 0,
        ease: Power2.easeOut,
        onComplete: this.doRoadSwitch
      }, "-=0.4").to(this.switchRoad, 0.4, {
        opacity: 1,
        ease: Power2.easeOut,
        onComplete: this.resetException
      }).to(this.copy3b, 0.4, {
        opacity: 1,
        y: 0,
        ease: Power2.easeOut
      }).to(this.noappPhone, 0.4, {
        opacity: 1,
        x: 0,
        ease: Power2.easeOut
      }, "-=0.4");
    };

    TechJourney.prototype.showEndpoints = function() {
      var tl;
      this.showEndpoints = this.noop;
      this.endpoints = this.endpointsWrap.children('.endpoints');
      tl = new TimelineLite();
      tl.staggerTo(this.endpoints, 0.4, {
        scale: 1,
        opacity: 1,
        ease: Back.easeOut.config(1.7)
      }, 0.1);
      if (this.isMobile) {
        return this.showCopy5();
      }
    };

    TechJourney.prototype.initDom = function() {
      this._window = $(window);
      this.steps = $('.steps');
      this.laptop = $('#tech-laptop');
      this.basketImg = $('#tech-basket');
      this.arrowImg = $('#tech-arrow');
      this.tokenImg = $('#tech-token');
      this.step3CopyWrap = $('#step-3-copy');
      this.step3Copy = this.step3CopyWrap.children();
      this.techToken2 = $('#tech-token2');
      this.techBank1 = $('#tech-bank1');
      this.techShop = $('#tech-shop');
      this.techTick = $('#tech-tick');
      this.techDownArrow = $('#tech-down-arrow');
      this.copy1 = $('#copy1');
      this.copy2 = $('#copy2');
      this.copy3a = $('#copy3a');
      this.copy3b = $('#copy3b');
      this.copy4 = $('#copy4');
      this.copy5 = $('#copy5');
      this.pingPhone = $('#tech-ping');
      this.noappPhone = $('#tech-noapp');
      this.yesBtn = $('button#yes');
      this.noBtn = $('button#no');
      this.switchRoad = $('.switch-road');
      this.step3 = $('#step3');
      this.step4 = $('#step4');
      this.endRoads0 = $('#step6 .road0 .prog-line');
      this.endRoads1a = $('#step6 .road1a .prog-line');
      this.endRoads1b = $('#step6 .road1b .prog-line');
      this.endRoads2a = $('#step6 .road2a .prog-line');
      this.endRoads2b = $('#step6 .road2b .prog-line');
      this.endRoads2c = $('#step6 .road2c .prog-line');
      return this.endpointsWrap = $('#tech-enpoint-wrap');
    };

    return TechJourney;

  })();

  this.TechJourney = new TechJourney;

}).call(this);
