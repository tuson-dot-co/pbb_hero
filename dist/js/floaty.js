(function() {
  var FloatyPhone,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  FloatyPhone = (function() {
    function FloatyPhone() {
      this.initDom = bind(this.initDom, this);
      this.oldBrowser = bind(this.oldBrowser, this);
      this.startBob = bind(this.startBob, this);
      this.isOldBrowser = this.oldBrowser();
      this.initDom();
      $((function(_this) {
        return function() {
          if (_this.isOldBrowser) {
            _this.phoneWrap.addClass('fallback');
            return TweenLite.to(_this.phoneWrap, 0.8, {
              y: 0,
              opacity: 1
            });
          } else {
            _this.TL = new TimelineMax({
              delay: 0
            });
            return _this.TL.to(_this.phoneWrap, 0.8, {
              y: 0,
              opacity: 1
            }).to(_this.phone, 0.8, {
              y: 0,
              rotationX: 0,
              rotationY: 0,
              rotation: 0,
              ease: Power1.easeOut
            }).to(_this.shadow, 0.8, {
              y: 8,
              rotationX: 0,
              rotationY: 0,
              rotation: 0,
              onComplete: _this.startBob,
              ease: Power1.easeOut
            }, "-=0.8");
          }
        };
      })(this));
    }

    FloatyPhone.prototype.startBob = function() {
      TweenMax.to(this.phone, 1.5, {
        y: -3,
        repeat: -1,
        yoyo: true,
        repeatDelay: 0,
        ease: Power1.easeInOut
      });
      return TweenMax.to(this.shadow, 1.5, {
        rotation: 2,
        repeat: -1,
        yoyo: true,
        opacity: 0.7,
        repeatDelay: 0,
        ease: Power1.easeInOut
      });
    };

    FloatyPhone.prototype.oldBrowser = function() {
      var Idx, sAgent;
      sAgent = window.navigator.userAgent;
      Idx = sAgent.indexOf("MSIE");
      if ((Idx > 0) || (!!navigator.userAgent.match(/Trident\/7\./))) {
        return true;
      } else {
        return false;
      }
    };

    FloatyPhone.prototype.initDom = function() {
      this.phoneWrap = $('#floating-phone-wrap');
      this.phone = $('#phone-device');
      return this.shadow = $('#phone-shadow');
    };

    return FloatyPhone;

  })();

  this.FloatyPhone = new FloatyPhone;

}).call(this);
