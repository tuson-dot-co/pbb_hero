module.exports = (grunt) ->

  grunt.initConfig
    sass:
      css:
        options:
          style: 'expanded'
        files:
          'dist/css/home-hero.css'  : 'src/css/home-hero.sass'
          'dist/css/tech.css'       : 'src/css/tech.sass'
          'dist/css/floaty.css'     : 'src/css/floaty.sass'
          'dist/css/retailers.css'  : 'src/css/retailers.sass'
          'dist/css/banks.css'      : 'src/css/banks.sass'
          'dist/css/consumers.css'  : 'src/css/consumers.sass'
          'dist/css/processors.css' : 'src/css/processors.sass'
          'dist/css/lazyload-demo.css' : 'src/css/lazyload-demo.sass'
      dist:
        options:
          style: 'compressed'
        files:
          'dist/css/home-hero.min.css'  : 'src/css/home-hero.sass'
          'dist/css/tech.min.css'       : 'src/css/tech.sass'
          'dist/css/floaty.min.css'     : 'src/css/floaty.sass'
          'dist/css/retailers.min.css'  : 'src/css/retailers.sass'
          'dist/css/banks.min.css'      : 'src/css/banks.sass'
          'dist/css/consumers.min.css'  : 'src/css/consumers.sass'
          'dist/css/processors.min.css' : 'src/css/processors.sass'
          'dist/css/lazyload-demo.min.css' : 'src/css/lazyload-demo.sass'

    coffee:
      compile:
        files:
          'dist/js/home-hero.js'    : 'src/js/home-hero.coffee'
          'dist/js/tech.js'         : 'src/js/tech.coffee'
          'dist/js/floaty.js'       : 'src/js/floaty.coffee'
          'dist/js/retailers.js'    : 'src/js/retailers.coffee'
          'dist/js/banks.js'        : 'src/js/banks.coffee'
          'dist/js/consumers.js'    : 'src/js/consumers.coffee'
          'dist/js/processors.js'   : 'src/js/processors.coffee'
          'dist/js/lazyload-demo.js'   : 'src/js/lazyload-demo.coffee'

    haml:
      dist:
        files:
          'dist/home-hero.html'           : 'src/home-hero.haml'
          'dist/tech.html'                : 'src/tech.haml'
          'dist/floating-phone.html'      : 'src/floating-phone.haml'
          'dist/retailers.html'           : 'src/retailers.haml'
          'dist/banks.html'               : 'src/banks.haml'
          'dist/consumers.html'           : 'src/consumers.haml'
          'dist/payment-processors.html'  : 'src/payment-processors.haml'
          'dist/lazyload-demo.html'       : 'src/lazyload-demo.haml'

    imagemin:
      files:
        expand: true,
        cwd: 'src/img',
        src: ['**/*.{png,jpg,gif,svg}'],
        dest: 'dist/img'

    watch:
      scripts:
        files: 'src/**/*.coffee',
        tasks: ['coffee']
      styles:
        files: 'src/**/*.sass',
        tasks: ['sass']
      html:
        files: 'src/**/*.haml',
        tasks: ['haml']
      img:
        files: 'src/**/*.img',
        tasks: ['imagemin']

    uglify:
      vendor:
        files:
          'dist/js/vendor.min.js': ['src/js/vendor/vivus.min.js']
      dist:
        files:
          'dist/js/home-hero.min.js'    : ['dist/js/vendor.min.js', 'dist/js/home-hero.js']
          'dist/js/tech.min.js'         : ['dist/js/tech.js']
          'dist/js/floaty.min.js'       : ['dist/js/floaty.js']
          'dist/js/retailers.min.js'    : ['dist/js/vendor.min.js', 'dist/js/retailers.js']
          'dist/js/banks.min.js'        : ['dist/js/vendor.min.js', 'dist/js/banks.js']
          'dist/js/consumers.min.js'    : ['dist/js/vendor.min.js', 'dist/js/consumers.js']
          'dist/js/processors.min.js'   : ['dist/js/vendor.min.js', 'dist/js/processors.js']
          'dist/js/lazyload-demo.min.js': ['dist/js/lazyload-demo.js']

  grunt.loadNpmTasks 'grunt-contrib-sass'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-haml'
  grunt.loadNpmTasks 'grunt-contrib-imagemin'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  # Default task(s).
  grunt.registerTask 'default', ['sass:css', 'coffee', 'haml', 'imagemin', 'uglify:vendor', 'watch']

  # Delpoy task(s).
  grunt.registerTask 'deploy', ['uglify:dist']

