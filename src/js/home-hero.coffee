class HomepageHero

  constructor: ->

    @isOldBrowser = @oldBrowser()

    @initDom()

    @isMobile = true if /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))

    $ =>

      if !@isOldBrowser
        # TweenMax SVG animation smoothing
        CSSPlugin.useSVGTransformAttr = true

        # Initialise Vivus SVG animations
        drawSpeed = 35
        @logoAnim   = new Vivus('main-logo-extension', { duration: drawSpeed, delay: 0 }).finish()
        @mobileAnim = new Vivus('mobile-surround', {type: 'oneByOne', duration: drawSpeed, delay: 0}).stop().reset()
        @coffeeAnim = new Vivus('coffee-cup', {type: 'oneByOne', duration: drawSpeed, delay: 0}).stop().reset()
        @pizzaAnim  = new Vivus('pizza', {type: 'oneByOne', duration: drawSpeed, delay: 0}).stop().reset()
        @finalLogoAnim  = new Vivus('final-logo-extension', {type: 'oneByOne', duration: drawSpeed, delay: 0}).stop().reset()

        TweenLite.set @drawingsContainer, {display: 'block'}

      else
        @mainLogo.remove()
        @svgs.remove()
        TweenLite.set @fallbackDrawings, {display: 'block'}

        
      # Get this party started
      TweenLite.to @heroContent, 0.4, {opacity: 1, onStart: @startHeroAnimation}

      # @pause.on('click', @pauseTL)
      # @play.on('click', @playTL)

  startHeroAnimation: =>

    # Final scale and X/Y coords dependent on platform
    @finalDims = {
      scale:  if @isMobile then 0.3 else 0.5
      y:      if @isMobile then 12  else 36
      x:      if @isMobile then -97 else -170
    }

    # Common timings in seconds
    stateDwellTime  = 2
    stateFadeTime   = 1
    repeatNum       = -1

    # Init new Timeline
    @TL = new TimelineMax({delay:0, repeat: repeatNum})#.pause()

    temp = 0.3

    # Animation chain
    @TL.from @initialState, stateFadeTime,   { autoAlpha: 0 }
      .from @mainLogo, stateFadeTime,       { autoAlpha: 0 }, "-=#{stateFadeTime}"
      .from @initialStateCopy, 0.3,         { autoAlpha: 0, y: 0, ease: Power2.easeOut }, "-=#{stateFadeTime}"
      .to   @mainLogo, temp,                { scale: 0.5, x: -60, y: 67, onStart: @undoMainLogo, ease: Power0.easeNone }, "+=#{stateDwellTime}"
      .to   @drawingsContainer, temp,       { y: 30, x: -20, ease: Power2.easeOut }, "-=#{temp}"
      # undo fallback 1
      .to   @fallback1, 0.3,                 { opacity: 0 }, "-=#{temp}"
      
      # .to   @drawingsContainer, 0.4,        { y: 30, x: -20, ease: Power0.easeNone }, "-=#{temp}"
      .set  @mobileWrapElements,            { x: 20 }, "-=0.3"
      .to   @initialStateCopy, 0.3,         { opacity: 0, y: 20, ease: Power2.easeOut }, "-=0.5"
      .to   @logoBase, 0.2,                 { opacity: 1 }
      .to   @mainLogo, 0.3,                 { opacity: 0, onComplete: @doMainLogo }, "-=0.2"
      

      .from @mobileState, stateFadeTime,    { autoAlpha: 0, onStart: @doMobile }#, "-=0.2"
      # do fallback 2
      .from @fallback2, 0.6,                { autoAlpha: 0 }, "-=#{stateFadeTime}"

      .from @mobileStateCopy, 0.1,          { autoAlpha: 0, y: 0 }, "-=#{stateFadeTime*0.5}"
      .from @mobileStateScreens, 0.6,       { autoAlpha: 0 }, "-=0.6"
      .to   @mobileScreen1, 0.6,            { opacity: 0 }, "+=1"
      # undo fallback 2 and do fallback 3
      .to   @fallback2, 0.6,                { opacity: 0 }, "-=0.6"
      .from @fallback3, 0.6,                { autoAlpha: 0 }, "-=0.6"
      
      .to   @mobileStateCopy, 0.3,          { opacity: 0, y: 20, onStart: @undoMobile }, "+=#{stateDwellTime}"
      .to   @fallback3, 0.6,                { opacity: 0 }, "-=0.3"
      .to   @mobileStateScreens, 0.3,       { opacity: 0 }, "-=0.6"

      .to   @drawingsContainer, 0.4,        { x: 25, y: 0 }, "-=0.3"
      .from @coffeeCupState, stateFadeTime, { autoAlpha: 0, onStart: @doCoffee }, "-=0.4"
      # do fallback 4
      .from @fallback4, 0.6,                { autoAlpha: 0 }, "-=#{stateFadeTime}"

      .set  @mobileState,                   { opacity: 0 }
      .from @coffeeCupStateCopy, 0.1,       { autoAlpha: 0, y: 0 }, "-=#{stateFadeTime*0.5}"
      .to   @coffeeCupStateCopy, 0.3,       { opacity: 0, y: 20, onStart: @undoCoffee }, "+=#{stateDwellTime}"
      # undo fallback 4
      .to   @fallback4, 0.6,                { opacity: 0 }, "-=0.3"

      .to   @drawingsContainer, 0.4,        { x: 0 }, "-=0.3"
      .from @pizzaState, stateFadeTime,     { autoAlpha: 0, onStart: @doPizza }, "-=0.4"
      # do fallback 5
      .from @fallback5, 0.6,                { autoAlpha: 0 }, "-=#{stateFadeTime}"

      .from @pizzaStateCopy, 0.1,           { autoAlpha: 0, y: 0 }, "-=#{stateFadeTime*0.5}"
      .to   @pizzaStateCopy, 0.3,           { opacity: 0, y: 20, onStart: @undoPizza }, "+=#{stateDwellTime}"
      # undo fallback 5
      .to   @fallback5, 0.6,                { opacity: 0 }, "-=0.3"

      .set  @coffeeCupState,                { opacity: 0 }
      
      .to   @drawingsContainer, 0.3,        { x: 0, y: 0 }, "-=0.3"
      .set  @logoBase,                      { opacity: 0 }
      .set  @finalLogo,                      { opacity: 1, y: 67, x: -60, scale: 0.5 }
      .to   @finalLogo, 0.3,                 { scale: @finalDims.scale, x: @finalDims.x, y: @finalDims.y, onStart: @doFinalLogo }
      .from @paymark, 0.4,                  { autoAlpha: 0 }
      # do fallback 6
      .from @fallback6, 0.6,                { autoAlpha: 0 }, "-=0.4"

      .from @finalStateCopy, 0.1,           { autoAlpha: 0, y: 0 }
      .to   @paymark, 0.4,                  { opacity: 0 }, "+=#{stateDwellTime*2}"
      .to   @finalLogo, 0.3,                 { opacity: 0 }, "-=0.4"
      # undo fallback 6
      .to   @fallback6, 0.6,                { opacity: 0 }, "-=0.4"
      
      .to   @finalStateCopy, 0.3,           { opacity: 0, y: 20}, "-=0.6"
      .to   @pizzaState, 1,                 { opacity: 0 }, "-=0.3"
      # .to   @finalLogo, 0.3,                 { scale: 1, x: 0, y: 0}, "-=1"

      # do fallback 1
      .to   @fallback1, 0.6,                { opacity: 1 }, "-=0.3"
      
    # @TL.progress(0.5)

  # pauseTL: =>
  #   @TL.pause()

  # playTL: =>
  #   @TL.play()

  # The Drawings
  doMainLogo: =>
    if !@isOldBrowser
      @logoAnim.play(1)
  undoMainLogo: =>
    if !@isOldBrowser
      @logoAnim.play(-3)

  doMobile: =>
    if !@isOldBrowser
      @mobileAnim.reset().play(1)
  undoMobile: =>
    if !@isOldBrowser
      @mobileAnim.play(-3)

  doCoffee: =>
    if !@isOldBrowser
      @coffeeAnim.reset().play(1)
  undoCoffee: =>
    if !@isOldBrowser
      @coffeeAnim.play(-3)

  doPizza: =>
    if !@isOldBrowser
      @pizzaAnim.reset().play(1)
  undoPizza: =>
    if !@isOldBrowser
      @pizzaAnim.play(-3)

  doFinalLogo: =>
    if !@isOldBrowser
      @finalLogoAnim.reset().play(1)
  undoFinalLogo: =>
    if !@isOldBrowser
      @finalLogoAnim.play(-3)


  oldBrowser: =>

    sAgent = window.navigator.userAgent
    Idx = sAgent.indexOf("MSIE")

    if (Idx > 0) || (!!navigator.userAgent.match(/Trident\/7\./))
      return true
    else 
      return false


  initDom: =>

    @pause = $('#pause')
    @play = $('#play')

    @heroContent = $('#hero-header')
    @heroStates = $('#hero-states-container .states')

    # State 1: Initial
    @initialState     = $('#initial-state')
    @initialStateCopy = $('#initial-state h3')

    # State 2: Mobile
    @mobileState   = $('#mobile-state')
    @mobileStateCopy = $('#mobile-state h3')
    @mobileWrapElements = $('.exception')
    @mobileStateScreens = $('#hero-phone-screens')
    @mobileScreen1 = $('#mob-screen-1')
    @mobileScreen2 = $('#mob-screen-2')

    # State 3: Coffee Cup
    @coffeeCupState  = $('#coffee-cup-state')
    @coffeeCupStateCopy = $('#coffee-cup-state h3')

    # State 4: pizza
    @pizzaState   = $('#pizza-state')
    @pizzaStateCopy = $('#pizza-state h3')

    # State 5: Final
    @finalState     = $('#final-state')
    @finalStateCopy = $('#final-state h3')
    @paymark        = $('#paymark')

    # Drawings
    @drawingsContainer = $('#drawings')
    @mainLogo          = $('#main-logo-wrap')
    @finalLogo         = $('#final-logo-wrap')
    @svgs              = $('.icon-elems')
    @logoBase          = $('#drawings-logo-base')

    @fallbackDrawings = $('#fallback-drawings')
    @fallback1        = @fallbackDrawings.children('#fallback1')
    @fallback2        = @fallbackDrawings.children('#fallback2')
    @fallback3        = @fallbackDrawings.children('#fallback3')
    @fallback4        = @fallbackDrawings.children('#fallback4')
    @fallback5        = @fallbackDrawings.children('#fallback5')
    @fallback6        = @fallbackDrawings.children('#fallback6')
    

@HomepageHero = new HomepageHero