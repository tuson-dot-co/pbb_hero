
class TechJourney

  constructor: ->
    
    @initDom()
    @innerHeight  = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || @_window.height()

    @isMobile = false
    # Device detection
    @isMobile = true if /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))

    # This variable allows deviation from the usual path flow
    @_EXCEPTION = false

    @_ORANGE = '#ff6e00'
    @_GREY   = '#898989'

    $ =>

      @showCopy1()

      TweenLite.defaultEase = Power0.easeNone

      # :: Step data calibration ::
      @calibrateSteps()

      # On resize, recalibrate the page height and step data, 
      # but only once resize has settled (leave it X milliseconds)
      @reCalibrateSteps = =>
        @doCalibrate = setTimeout( =>
          @calibrateSteps()
        , 700)

      $(window).resize =>
        clearTimeout @doCalibrate
        @reCalibrateSteps()

      @configureProgressAnimations()
      
      @stepThrottled = {}

      # Check scroll position every 200ms
      @doChanges()
      setInterval( =>
        @doChanges()
      , 100)


      if @isMobile
        # @nonAffirmativeBtn.on('click', @toggleRoute)

        @yesBtn.on('click', @showRoute3A)
        @noBtn.on('click', @showRoute3B)


  noop: =>


  calibrateSteps: =>

    @innerHeight  = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || @_window.height()

    @stepData = {}

    for step in @steps
      stepNum = $(step).attr('data-step-num')

      @stepData[stepNum] =
        offsetTop:    $(step).offset().top
        height:       $(step).height()
        offsetBottom: $(step).offset().top + $(step).height()

  switchThrottleTo: (num) =>
    for step, i in @steps
      if num is i+1
        @stepThrottled[i+1] = true
      else
        @stepThrottled[i+1] = false

  doChanges: =>

    # Top & Bottom of page position
    t = (window.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0)
    b = t + @innerHeight

    _offset = 150

    # GO TO STEP
    if (t < @stepData[1].offsetTop - _offset) and @stepThrottled[1] != true
      @switchThrottleTo(1)
      @_PROGRESS = 1
      return @TL.tweenTo('step 1', {onComplete: @wobbleMarker })

    if (t > @stepData[1].offsetTop - _offset) and (t < @stepData[2].offsetTop - _offset) and @stepThrottled[2] != true
      @switchThrottleTo(2)
      @_PROGRESS = 2
      @showToken()
      @showCopy2()
      return @TL.tweenTo('step 2', {onComplete: @wobbleMarker })

    if (t > @stepData[2].offsetTop - _offset) and (t < @stepData[3].offsetTop - _offset) and @stepThrottled[3] != true
      @switchThrottleTo(3)
      @_PROGRESS = 3
      @flattenButtons()
      return @TL.tweenTo('step 3', {onComplete: @step3stuff })

    if (t > @stepData[3].offsetTop - _offset) and (t < @stepData[4].offsetTop - _offset) and @stepThrottled[4] != true and @_EXCEPTION != true
      @switchThrottleTo(4)
      @_PROGRESS = 4
      @colouriseButton()
      @showCopy3a()
      @showCopy3b(0.6) if !@isMobile
      return @TL.tweenTo('step 4', {onComplete: @wobbleMarker })

    if (t > @stepData[4].offsetTop - _offset) and (t < @stepData[5].offsetTop - _offset) and @stepThrottled[5] != true
      @switchThrottleTo(5)
      @_PROGRESS = 5
      @showTransaction()
      @showCopy4()
      return @TL.tweenTo('step 5', {onComplete: @wobbleMarker })

    if (t > @stepData[5].offsetTop - _offset) and (t < (@stepData[6].offsetTop - (@stepData[5].height * 0.7) ) ) and @stepThrottled[6] != true
      @switchThrottleTo(6)
      @_PROGRESS = 6
      return @TL.tweenTo('finish', {onComplete: @finishStuff })

  finishStuff: =>
    @showEndpoints()

  step3stuff: =>
    @wobbleMarker()
    @step3infoBox()

  step3infoBox: =>
    @step3infoBox = @noop
    anim  = new TimelineMax({delay:0.2})
    spd   = 0.4
    anim.to @step3CopyWrap, spd, {scale: 1, opacity: 1, ease: Power2.easeOut}
      .to @step3Copy, spd, {opacity: 1, y: 0, ease: Power2.easeOut}

  wobbleMarker: =>

    if @_PROGRESS < 7
      el = $("#marker#{@_PROGRESS}")
      TweenMax.to el, .1, { scale: 1.1, ease: Quad.easeInOut }
      TweenMax.to el, .1, { scale: 1, delay: .1 }

    if @_PROGRESS is 6 && !@isMobile
      
      @showCopy5( 2-2 )

      el = $("#marker6dt")
      TweenMax.to el, .1, { scale: 1.1, ease: Quad.easeInOut }
      TweenMax.to el, .1, { scale: 1, delay: .1 }


  configureProgressAnimations: =>

    @TL = new TimelineMax({delay:0}).pause()

    _speed = 0.12

    for step, i in @steps

      @TL.addLabel("step #{i+1}")

      if (i+1) < 6

        for road in $(step).children('.roads')

          _road = $(road)
          _line = _road.children('.prog-line')

          if _road.hasClass 'vert'
            @TL.append TweenLite.to _line, _speed, {height: '100%'}
          else
            @TL.append TweenLite.to _line, _speed, {width: '100%'}

      else if (i+1) is 6

        if @isMobile
          @TL.append TweenLite.to @endRoads1a, _speed, { width: '100%', onStart: @wobbleMarker }
          @TL.append TweenLite.to @endRoads1b, _speed, { width: '100%', delay: -_speed }
          @TL.append TweenLite.to @endRoads2a, _speed, { height: '100%' }
          @TL.append TweenLite.to @endRoads2b, _speed, { height: '100%', delay: -_speed }
          @TL.append TweenLite.to @endRoads2c, _speed, { height: '100%', delay: -_speed }

        else
          @TL.append TweenLite.to @endRoads0, _speed, { height: '100%', onStart: @wobbleMarker }
          @TL.append TweenLite.to @endRoads1a, _speed, { width: '100%' }
          @TL.append TweenLite.to @endRoads1b, _speed, { width: '100%', delay: -_speed }
          @TL.append TweenLite.to @endRoads2a, _speed, { height: '100%' }
          @TL.append TweenLite.to @endRoads2b, _speed, { height: '100%', delay: -_speed*2 }
          @TL.append TweenLite.to @endRoads2c, _speed, { height: '100%', delay: -_speed }

        @TL.addLabel("finish")


  showTransaction: =>
    @showTransaction = @noop

    anim  = new TimelineMax({delay:1})
    spd   = 0.6

    anim.to @techBank1, 0.4, {y: 0, opacity: 1, ease: Power2.easeOut}
      .to @techShop, 0.4, {y: 0, opacity: 1, ease: Power2.easeOut}
      .set @techToken2, {opacity: 1}
      .to @techToken2, 1.8, {y: '300%', rotation: 35, ease: Power3.easeIn}, "-=0.4"
      .to @techToken2, 0.6, {y: '195%', rotation: 0, ease: Back.easeOut.config(1.7)}
      .to @techTick, 0.3, {scale: 1, opacity: 1, ease: Back.easeOut.config(1.7)}, "-=0.6"
      .to @techDownArrow, 0.3, {opacity: 1, y: 0, ease: Power2.easeOut}, "+=0.3"


  showToken: =>
    @showToken = @noop

    anim  = new TimelineMax({delay:0})
    spd   = 0.6

    anim.to @basketImg, spd,  { opacity: 1, x: 0, ease: Power2.easeOut }
      .to @arrowImg, spd,     { opacity: 1, x: 0, ease: Power2.easeOut }, "-=0.3"
      .to @tokenImg, spd,     { opacity: 1, x: 0, ease: Power2.easeOut }, "-=0.3"

  showCopy1: =>
    @showCopy1 = @noop
    TweenLite.to @laptop, 0.7, {opacity: 1, y: 0, ease: Power2.easeOut}
    TweenLite.to @copy1, 0.7, {delay: 0.5, opacity: 1, y: 0, ease: Power2.easeOut}

  showCopy2: =>
    @showCopy2 = @noop
    TweenLite.to @copy2, 0.7, {delay: 0.6, opacity: 1, y: 0, ease: Power2.easeOut}

  showCopy3a: =>
    @showCopy3a = @noop
    TweenLite.to @copy3a, 0.7, {delay: 0.6, opacity: 1, y: 0, ease: Power2.easeOut}
    TweenLite.to @pingPhone, 0.7, {delay: 1, opacity: 1, x: 0, ease: Power2.easeOut}

  showCopy3b: (dly) =>
    dly ?= 0
    @showCopy3b = @noop
    TweenLite.to @copy3b, 0.7, {delay: dly, opacity: 1, y: 0, ease: Power2.easeOut}
    if @isMobile
      TweenLite.to @noappPhone, 0.7, {delay: 0.6, opacity: 1, x: 0, ease: Power2.easeOut}

  showCopy4: =>
    @showCopy4 = @noop
    TweenLite.to @copy4, 0.7, {delay: 0.6, opacity: 1, y: 0, ease: Power2.easeOut}

  showCopy5: (dly) =>
    dly ?= 1
    @showCopy5 = @noop
    TweenLite.to @copy5, 0.7, {delay: dly, opacity: 1, y: 0, ease: Power2.easeOut}



  resetException: =>
    @_EXCEPTION = false
    # @doChanges()

  doRoadSwitch: =>
    if @step3.hasClass 'route-a'
      @step3.removeClass 'route-a'
      @step3.addClass 'route-b'
      @step4.removeClass 'route-a'
      @step4.addClass 'route-b'
    else
      @step3.addClass 'route-a'
      @step3.removeClass 'route-b'
      @step4.removeClass 'route-b'
      @step4.addClass 'route-a'
      

  flattenButtons: =>
    TweenLite.to $('button.options'), 0.1, {css:{backgroundColor: @_GREY }}

  colouriseButton: =>
    TweenLite.to $('button.options.selected'), 0.2, {delay: 0.1, css:{ backgroundColor: @_ORANGE }}
      

  showRoute3A: =>
    return if @step3.hasClass 'route-a'
    
    @noBtn.removeClass 'selected'
    @yesBtn.addClass 'selected'

    @_EXCEPTION = true

    @flattenButtons()

    @switchThrottleTo(3)
    @_PROGRESS = 3
    @TL.tweenTo('step 3').duration(0.15)

    anim = new TimelineLite({delay: 0.1})

    anim.to @copy3b, 0.4, {opacity: 0, y: -10, ease: Power2.easeOut}
      .to @noappPhone, 0.4, {opacity: 0, x: -60, ease: Power2.easeOut}, "-=0.4"
      .to @switchRoad, 0.4, {opacity: 0, ease: Power2.easeOut, onComplete: @doRoadSwitch}, "-=0.4"
      .to @switchRoad, 0.4, {opacity: 1, ease: Power2.easeOut, onComplete: @resetException}
      .to @copy3a, 0.4, {opacity: 1, y: 0, ease: Power2.easeOut}
      .to @pingPhone, 0.4, {opacity: 1, x: 0, ease: Power2.easeOut}, "-=0.4"



  showRoute3B: =>
    return if @step3.hasClass 'route-b'
    @_EXCEPTION = true

    @flattenButtons()

    @yesBtn.removeClass 'selected'
    @noBtn.addClass 'selected'

    @switchThrottleTo(3)
    @_PROGRESS = 3
    @TL.tweenTo('step 3').duration(0.15)

    anim = new TimelineLite({delay: 0.1})

    anim.to @copy3a, 0.4, {opacity: 0, y: -10, ease: Power2.easeOut}
      .to @pingPhone, 0.4, {opacity: 0, x: 60, ease: Power2.easeOut}, "-=0.4"
      .to @switchRoad, 0.4, {opacity: 0, ease: Power2.easeOut, onComplete: @doRoadSwitch}, "-=0.4"
      .to @switchRoad, 0.4, {opacity: 1, ease: Power2.easeOut, onComplete: @resetException}
      .to @copy3b, 0.4, {opacity: 1, y: 0, ease: Power2.easeOut}
      .to @noappPhone, 0.4, {opacity: 1, x: 0, ease: Power2.easeOut}, "-=0.4"
      


  showEndpoints: =>
    @showEndpoints = @noop
    @endpoints = @endpointsWrap.children('.endpoints')
    tl = new TimelineLite()
    tl.staggerTo @endpoints, 0.4, {scale: 1, opacity: 1, ease:  Back.easeOut.config(1.7)}, 0.1
    
    if @isMobile then @showCopy5()


  initDom: =>

    @_window = $(window)
    @steps = $('.steps')

    # Various page animations
    @laptop     = $('#tech-laptop')

    @basketImg  = $('#tech-basket')
    @arrowImg   = $('#tech-arrow')
    @tokenImg   = $('#tech-token')

    @step3CopyWrap  = $('#step-3-copy')
    @step3Copy      = @step3CopyWrap.children()

    @techToken2     = $('#tech-token2')
    @techBank1      = $('#tech-bank1')
    @techShop       = $('#tech-shop')
    @techTick       = $('#tech-tick')
    @techDownArrow  = $('#tech-down-arrow')

    # Copy items
    @copy1  = $('#copy1')
    @copy2  = $('#copy2')
    @copy3a = $('#copy3a')
    @copy3b = $('#copy3b')
    @copy4  = $('#copy4')
    @copy5  = $('#copy5')

    @pingPhone = $('#tech-ping')
    @noappPhone = $('#tech-noapp')

    # Route 3 Options
    @yesBtn = $('button#yes')
    @noBtn  = $('button#no')

    # @optionsWrap = $('#step-3-options')

    # @selectedOption = $('.options.selected')

    @switchRoad = $('.switch-road')

    @step3  = $('#step3')
    @step4  = $('#step4')

    @endRoads0  = $('#step6 .road0 .prog-line') # Desktop only
    @endRoads1a = $('#step6 .road1a .prog-line')
    @endRoads1b = $('#step6 .road1b .prog-line')

    @endRoads2a = $('#step6 .road2a .prog-line')
    @endRoads2b = $('#step6 .road2b .prog-line')
    @endRoads2c = $('#step6 .road2c .prog-line')

    @endpointsWrap  = $('#tech-enpoint-wrap')


@TechJourney = new TechJourney