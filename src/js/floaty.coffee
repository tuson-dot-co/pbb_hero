class FloatyPhone

  constructor: ->

    @isOldBrowser = @oldBrowser()

    @initDom()

    $ =>

      if @isOldBrowser
        @phoneWrap.addClass 'fallback'
        TweenLite.to @phoneWrap, 0.8, { y: 0, opacity: 1 }
      else
        @TL = new TimelineMax({delay: 0})

        @TL.to @phoneWrap, 0.8, { y: 0, opacity: 1 }
          .to @phone, 0.8, { y: 0, rotationX: 0, rotationY: 0, rotation: 0, ease: Power1.easeOut}
          .to @shadow, 0.8, { y: 8, rotationX: 0, rotationY: 0, rotation: 0, onComplete: @startBob, ease: Power1.easeOut}, "-=0.8"


  startBob: =>
    TweenMax.to @phone, 1.5, { y: -3, repeat: -1, yoyo:true, repeatDelay:0, ease: Power1.easeInOut}
    TweenMax.to @shadow, 1.5, { rotation: 2, repeat: -1, yoyo:true, opacity: 0.7, repeatDelay:0, ease: Power1.easeInOut}

  oldBrowser: =>

    sAgent = window.navigator.userAgent
    Idx = sAgent.indexOf("MSIE")

    if (Idx > 0) || (!!navigator.userAgent.match(/Trident\/7\./))
      return true
    else 
      return false


  initDom: =>

    @phoneWrap  = $('#floating-phone-wrap')
    @phone      = $('#phone-device')
    @shadow     = $('#phone-shadow')



@FloatyPhone = new FloatyPhone