class LazyDemo
  constructor: ->

    @initDom()

    TweenLite.defaultEase = Expo.easeOut

    $ =>

      @sectionThrottled = {}

      @calibrateSections()

      @reCalibrateSections = =>
        @doCalibrate = setTimeout( =>
          @calibrateSections()
        , 700)

      $(window).resize =>
        clearTimeout @doCalibrate
        @reCalibrateSections()

      @doChanges()
      setInterval( =>
        @doChanges()
      , 100)

  noop: =>


  calibrateSections: =>

    @innerHeight  = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || @_window.height()

    @sectionData = {}

    for section in @sections
      sectionNum = $(section).attr('data-section-num')

      @sectionData[sectionNum] =
        offsetTop:    $(section).offset().top
        height:       $(section).height()
        offsetBottom: $(section).offset().top + $(section).height()


  switchThrottleTo: (num) =>
    for sec, i in @sections
      if num is i+1
        @sectionThrottled[i+1] = true
      else
        @sectionThrottled[i+1] = false

  doChanges: =>

    # Top & Bottom of page position
    t = (window.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0)
    b = t + @innerHeight

    _offset = 200

    # GO TO STEP
    if (b > @sectionData[1].offsetBottom - _offset) and (b < @sectionData[2].offsetBottom - _offset) and @sectionThrottled[1] != true
      @switchThrottleTo(1)
      return @doSection1()
    if (b > @sectionData[2].offsetBottom - _offset) and (b < @sectionData[3].offsetBottom - _offset) and @sectionThrottled[2] != true
      @switchThrottleTo(2)
      return @doSection2()
    if (b > @sectionData[3].offsetBottom - _offset) and @sectionThrottled[3] != true
      @switchThrottleTo(3)
      return @doSection3()


  doSection1: =>
    @doSection1 = @noop

    graphicSpd  = 2
    copySpd     = 0.7

    TL = new TimelineMax({delay: 0})

    TL.to @graphics1, graphicSpd, { opacity: 1 }
      .to @lazyphone1, graphicSpd, { x: 0 }, "-=#{graphicSpd}"
      .to @lazyphone2, graphicSpd, { opacity: 1, x: 0 }, "-=#{graphicSpd}"
      .to @copy1, copySpd, { opacity: 1, y: 0 }, "-=#{graphicSpd*0.5}"
      .to @cta, copySpd, { delay: 0.8, opacity: 1, x: 0 }, "-=#{graphicSpd*0.8}"

  doSection2: =>
    @doSection2 = @noop

    graphicSpd  = 2
    copySpd     = 0.7

    TL = new TimelineMax({delay: 0})

    TL.to @lazyphone3, graphicSpd, { opacity: 1, x: 0 }
      .to @copy2, copySpd, { opacity: 1, y: 0 }, "-=#{graphicSpd*0.8}"

  doSection3: =>
    @doSection3 = @noop

    graphicSpd  = 2
    copySpd     = 0.7

    TL = new TimelineMax({delay: 0})

    TL.to @lazyphone4, graphicSpd, { opacity: 1, x: 0 }
      .to @copy3, copySpd, { opacity: 1, y: 0 }, "-=#{graphicSpd*0.8}"







  initDom: =>

    @sections   = $('.lazy-section')

    @lazyphone1 = $('#lazyphone1')
    @lazyphone2 = $('#lazyphone2')
    @lazyphone3 = $('#lazyphone3')
    @lazyphone4 = $('#lazyphone4')

    @graphics1  = $('#graphics1')
    @copy1      = $('#copy1')
    @copy2      = $('#copy2')
    @copy3      = $('#copy3')
    @cta        = $('#cta')




@LazyDemo = new LazyDemo