 /$$   /$$ /$$       /$$
| $$  | $$|__/      | $$
| $$  | $$ /$$      | $$
| $$$$$$$$| $$      | $$
| $$__  $$| $$      |__/
| $$  | $$| $$          
| $$  | $$| $$       /$$
|__/  |__/|__/      |__/
                        


This repo contains specific features of the Pay by Bank site as individual html files, with corresponding CSS and JS dependencies.

Please do not hesitate to call me if anything seems wrong or is not going smoothly:
(m): 0779 563 4500
(e): joe@tuson.co

- - - -

:: CONTENTS ::

Although the repo is called “pbb_hero”, it contains more features than just one hero section.

So far, the features coded are:
- Homepage hero
- How it Works: Technology journey section
- Why Retailers Hero
- Why Banks Hero
- Why Processors Hero
- Why Consumers Hero
- Floating phone animation to be used in multiple places on the site

There are still a few more features TBC, that I will push to this repo once completed.

- - - -

:: STRUCTURE ::

The structure is very basic. As an example of how the features are structured, you will find the code and image assets for the Homepage Hero as below:

/dist
	home-hero.html

/dist/css
	home-hero.css

/dist/js
	{ some version of tweenmax }
	home-hero.js

/dist/img
	{ various image assets as required by html and css, filenames mostly structured similar to “home-xxxxx.jpg/png” }

- - - -

:: IMPORTANT NOTES ::

1. During development, I have not had access to or used the correct specified fonts, however, I have tried to match the typographical tags to similar ones on the staging site (I imagine you will have global stylesheets that define the typography at a high level). Therefore, typography positioning, sizings and weights may need to be amended slightly by PE.

2. In order to help development of these features I needed to set some basic helper styles on various elements including html and body tags, and hero anchor tags. I have tried, for the most part, to keep these styles at the very top of each stylesheet - These can/should be deleted when integrated into the main site.

3. Image paths in the html are of course relative to the repo folder structure. These will need to be amended accordingly.

4. I have referenced TweenMax.min.js (v. 1.18.3) and Jquery (3.0.0-beta1) from Cloudflare CDN. I note that the staging site already includes both TweenMax and Jquery, so these can be omitted as script dependencies.


I have pushed both dist and src folders to the repo - so you can see my source code, or even edit and rebuild with grunt if needed.






